# -*- coding: utf-8 -*-
import socket, sys, zlib, httplib
import string
from xml.dom.minidom import parseString
import difflib
import lxml

language_dict = { "Japanese": "ja", "English": "en", "Romaji": "x-jat", "French": "fr" }

class PandaAgent(Agent.TV_Shows):
  #Required By Plex
  name = 'Panda'
  connection = None
  languages = [
    Locale.Language.English,
  ]
  primary_provider = True
  fallback_agent = False
  time = Datetime.Now()
  sem = Thread.Semaphore('panda_anidb_timer')
  
  #Plex Search Function
  #Current Presumes all titles are in English though language doesn't matter it returns, it could potentally get the other titles as giving by the Search API
  def search(self, results, media, lang, manual):
    Log('Searching...')
    msg = media.show
    Log('For: %s' %media.show)
    searchtitle = media.show.replace('The ', '')
    searchtitle = searchtitle.replace('This ', '')
    output = XML.ElementFromURL("http://anisearch.outrance.pl/?task=search&query=%s" %searchtitle)
    for show in output:
      score = 0
      name = ''
      for title in show:
        seq1 = title.text
        seq2 = media.show
        searchscore = int(difflib.SequenceMatcher(a=seq1.lower(), b=seq2.lower()).ratio() * 100)
        if score < searchscore:
          score = searchscore
          name = seq1
      results.Append(MetadataSearchResult(id=show.get('aid'),name=name+' ('+show.get('aid')+')',score=score,lang=Locale.Language.English))
    Log('Completed Search')
    return

  #Plex Update Function
  def update(self, metadata, media, lang, force = False):
    Log("Trying to acquire")
    self.sem.acquire()
    Log("Acquired")
    while (Datetime.Now() - self.time).seconds < 5:
      wait_time = (Datetime.Now() - self.time).seconds
    Log('Updating...')
    tvdb_found = False    
    Log('AniDB ID: %s' %metadata.id)
    oldtitle = metadata.title
    anidb = XML.ElementFromURL("http://api.anidb.net:9001/httpapi?request=anime&client=pandahttp&clientver=2&protover=1&aid=%s" %metadata.id, encoding='utf8')
    self.time = Datetime.Now()
    self.sem.release()
    try:
      english_title = anidb.xpath('//anime/titles/title[contains(@type,"official") and contains(@xml:lang,"en")]/text()')[0]
    except:
      english_title = anidb.xpath('//anime/titles/title/text()')[0]
    tvdb = XML.ElementFromURL("http://thetvdb.com/api/GetSeries.php?seriesname=%s" %english_title.replace(' ','%20'), encoding='utf8')
    if len(tvdb) > 0:
      tvdb_found = True
      tvdb_full = XML.ElementFromURL("http://thetvdb.com/api/A18D659A03358437/series/%s/all/" %tvdb.xpath('//Data/Series/seriesid/text()')[0], encoding='utf8')
      tvdb_banners = XML.ElementFromURL("http://thetvdb.com/api/A18D659A03358437/series/%s/banners.xml" %tvdb.xpath('//Data/Series/seriesid/text()')[0], encoding='utf8')
      metadata.studio = tvdb.xpath('//Data/Series/Network/text()')[0]
    if len(anidb.xpath('//anime/resources/resource[contains(@type,"1")]/externalentity/identifier/text()')) == 1:
      ann = XML.ElementFromURL("http://cdn.animenewsnetwork.com/encyclopedia/api.xml?anime=%s" %anidb.xpath('//anime/resources/resource[contains(@type,"1")]/externalentity/identifier/text()')[0], encoding='utf8')
    else:
      possible_ann_set = anidb.xpath('//anime/resources/resource[contains(@type,"1")]/externalentity/identifier/text()')
      probable_ann_id = possible_ann_set[0]
      for possible_ann_id in possible_ann_set:
        if possible_ann_id < probable_ann_id:
          probable_ann_id = possible_ann_id
      ann = XML.ElementFromURL("http://cdn.animenewsnetwork.com/encyclopedia/api.xml?anime=%s" %probable_ann_id, encoding='utf8')
    try:
      metadata.genres = ann.xpath('//ann/anime/info[contains(@type,"Genres")]/text()')
    except:
      Log('No Genres')
    #metadata.tags = anidb.xpath('//anime/categories/category/name/text()')
    try:
      metadata.collections = anidb.xpath('//anime/categories/category/name/text()')
    except:
      Log('No categories')
    try:
      if ann.xpath('//ann/anime/info[contains(@type,"Running time")]/text()'):
        if ann.xpath('//ann/anime/info[contains(@type,"Running time")]/text()')[0] == 'half hour':
          metadata.duration = 30 * 60000
        else:
          metadata.duration = int(ann.xpath('//ann/anime/info[contains(@type,"Running time")]/text()')[0]) * 60000
    except:
      Log('No running time')
    #metadata.rating float from 0 to 10
    PreferencedLanguage = Prefs['title_lang']
    Log.Info('Pref Output = %s', language_dict.get(PreferencedLanguage))
    try:
      metadata.title = anidb.xpath('//anime/titles/title[contains(@type,"official") and contains(@xml:lang,$translation)]/text()',translation = language_dict.get(PreferencedLanguage))[0]
    except:
      english_title = anidb.xpath('//anime/titles/title/text()')[0]
    try:
      metadata.summary = anidb.xpath('//anime/description/text()')[0]
      metadata.seasons[1].summary = anidb.xpath('//anime/description/text()')[0]
    except:
      Log('No summary')
    try:
      metadata.originally_available_at = Datetime.ParseDate(anidb.xpath('//anime/startdate/text()')[0])
    except:
      Log('No release date')
    try:
      metadata.content_rating = ann.xpath('//ann/anime/info[contains(@type,"Objectionable content")]/text()')[0]
    except:
      Log('No content rating')
    metadata.countries.clear()
    metadata.countries.add('Japan')
    try:
      url = 'http://img7.anidb.net/pics/anime/'+anidb.xpath('//anime/picture/text()')[0]
      metadata.posters[url] = Proxy.Media(HTTP.Request(url).content)
    except:
      Log('No poster on AniDB')
    if tvdb_found:
      for banner in tvdb_banners.xpath('//Banners/Banner'):
        if banner.xpath('./BannerType/text()')[0] == 'fanart':
          try:
            metadata.art['http://thetvdb.com/banners/'+banner.xpath('./BannerPath/text()')[0]] = Proxy.Preview(HTTP.Request('http://thetvdb.com/banners/'+banner.xpath('./ThumbnailPath/text()')[0]).content)
          except:
            metadata.art['http://thetvdb.com/banners/'+banner.xpath('./BannerPath/text()')[0]] = Proxy.Media(HTTP.Request('http://thetvdb.com/banners/'+banner.xpath('./BannerPath/text()')[0]).content)
        if banner.xpath('./BannerType/text()')[0] == 'poster':
          try:
            metadata.posters['http://thetvdb.com/banners/'+banner.xpath('./BannerPath/text()')[0]] = Proxy.Preview(HTTP.Request('http://thetvdb.com/banners/'+banner.xpath('./ThumbnailPath/text()')[0]).content)
          except:
            metadata.posters['http://thetvdb.com/banners/'+banner.xpath('./BannerPath/text()')[0]] = Proxy.Media(HTTP.Request('http://thetvdb.com/banners/'+banner.xpath('./BannerPath/text()')[0]).content)
        if banner.xpath('./BannerType/text()')[0] == 'series':
          try:
            metadata.banners['http://thetvdb.com/banners/'+banner.xpath('./BannerPath/text()')[0]] = Proxy.Preview(HTTP.Request('http://thetvdb.com/banners/'+banner.xpath('./ThumbnailPath/text()')[0]).content)
          except:
            metadata.banners['http://thetvdb.com/banners/'+banner.xpath('./BannerPath/text()')[0]] = Proxy.Media(HTTP.Request('http://thetvdb.com/banners/'+banner.xpath('./BannerPath/text()')[0]).content)
        if banner.xpath('./BannerType/text()')[0] == 'season':
          try:
            metadata.seasons[1].posters['http://thetvdb.com/banners/'+banner.xpath('./BannerPath/text()')[0]] = Proxy.Preview(HTTP.Request('http://thetvdb.com/banners/'+banner.xpath('./ThumbnailPath/text()')[0]).content)
          except:
            metadata.seasons[1].posters['http://thetvdb.com/banners/'+banner.xpath('./BannerPath/text()')[0]] = Proxy.Media(HTTP.Request('http://thetvdb.com/banners/'+banner.xpath('./BannerPath/text()')[0]).content)
    #metadata.seasons[1].posters[url] = Proxy.Media(HTTP.Request(url).content)
    #metadata.seasons[x].banners
    for episode in anidb.xpath('//anime/episodes/episode'):
      try:
        if episode.xpath('./epno/@type')[0] == '1':
          number = int(episode.xpath('./epno/text()')[0])
          if episode.xpath('./title[contains(@xml:lang,"ja")]'):
            metadata.seasons[1].episodes[number].title = episode.xpath( './title[contains(@xml:lang,"ja")]/text()')[0]
          else:
            metadata.seasons[1].episodes[number].title = episode.xpath('./title[contains(@xml:lang,"en")]/text()')[0]
          date = episode.xpath('./airdate/text()')[0]
          date_lookup = "//Data/Episode/FirstAired[contains(.,'"+date+"')]"
          metadata.seasons[1].episodes[number].originally_available_at = Datetime.ParseDate(date)
          #rating
          #metadata.seasons[1].episodes[number].writers = SET STRING 
          metadata.seasons[1].episodes[number].directors = anidb.xpath('//anime/creators/name[contains(@type,"Direction")]/text()')
          #producers set
          metadata.seasons[1].episodes[number].absolute_index = number
          #thumbs WON'T IMPLIMENT, TAKE FROM VIDEO FILE
          metadata.seasons[1].episodes[number].duration = int(episode.xpath('./length/text()')[0])*60000
          if tvdb_found:
            tvdb_ep = tvdb_full.xpath(date_lookup)
            if tvdb_ep:
              metadata.seasons[1].episodes[number].summary = tvdb_ep[0].xpath('../Overview/text()')[0]
            else:
              Log('Cannot find episdoe for date %s' %date)
          else:
            metadata.seasons[1].episodes[number].summary = anidb.xpath('//anime/description/text()')[0]
        else:
          text = episode.xpath('./epno/text()')[0]
          number = int(text.strip('S').strip('C').strip('T').strip('P'))
          season = int(episode.xpath('./epno/@type')[0])
          try:
            metadata.seasons[season].episodes[number].title = episode.xpath( './title[contains(@xml:lang,"ja")]/text()')[0]
          except:
            metadata.seasons[season].episodes[number].title = episode.xpath('./title[contains(@xml:lang,"en")]/text()')[0]
          date = episode.xpath('./airdate/text()')[0]
          date_lookup = "//Data/Episode/FirstAired[contains(.,'"+date+"')]"
          metadata.seasons[season].episodes[number].originally_available_at = Datetime.ParseDate(date)
          metadata.seasons[season].episodes[number].directors = anidb.xpath('//anime/creators/name[contains(@type,"Direction")]/text()')
          metadata.seasons[season].episodes[number].absolute_index = number
          metadata.seasons[season].episodes[number].duration = int(episode.xpath('./length/text()')[0])*60000
      except IndexError:
        Log("Don't have episode %s" %number)
    Log('Completed Update')
    return

#Initialzation function
def Start():
  Log("Initializing...")
  Log("Language test: ążaz你好")
  Log("Initialized")
  return

